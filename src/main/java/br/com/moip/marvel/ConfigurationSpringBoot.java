package br.com.moip.marvel;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;

@SpringBootApplication
@ComponentScan
@EnableAutoConfiguration
public class ConfigurationSpringBoot {

	public static void main(String[] args) {
		SpringApplication.run(ConfigurationSpringBoot.class, args);
	}
}
