package br.com.moip.marvel.domain;

public enum FormPayment {

	_AVISTA("Pagamento à vista"), 
	_2X("Pagamento em 02 vezes"),
	_3X("Pagamento em 03 vezes"),
	_5X("Pagamento em 05 vezes");
	
	private String form;
	
	FormPayment(String form) {
		this.form = form;
	}

	public String getForm() {
		return form;
	}

	public void setForm(String form) {
		this.form = form;
	}
}
