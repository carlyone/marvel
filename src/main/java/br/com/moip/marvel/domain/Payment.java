package br.com.moip.marvel.domain;

import org.hibernate.validator.constraints.NotEmpty;

public class Payment {

	@NotEmpty(message = "Favor preencher nome!")
	private String firstname;
	
	@NotEmpty(message = "Favor preenhcer sobrenome!")
	private String lastname;
	
	@NotEmpty(message = "Favor preencher CPF!")
	private String cpf;
	
	@NotEmpty(message = "Favor prencher número do cartão!")
	private String numbercard;
	
	private String cuponCode;	
	
	public String getFirstname() {
		return firstname;
	}
	public void setFirstname(String firstname) {
		this.firstname = firstname;
	}
	public String getLastname() {
		return lastname;
	}
	public void setLastname(String lastname) {
		this.lastname = lastname;
	}
	public String getCpf() {
		return cpf;
	}
	public void setCpf(String cpf) {
		this.cpf = cpf;
	}
	public String getNumbercard() {
		return numbercard;
	}
	public void setNumbercard(String numbercard) {
		this.numbercard = numbercard;
	}
	public String getCuponCode() {
		return cuponCode;
	}
	public void setCuponCode(String cuponCode) {
		this.cuponCode = cuponCode;
	}
	
}
