package br.com.moip.marvel.domain;

public class PaymentResponse {

	private Long id_transacao;
	private Double valor;
	private Integer status_pagamento;
	private Long cod_moip;
	private Integer forma_pagamento;
	private String tipo_pagamento;
	private String email_consumidor;
	
	public Long getId_transacao() {
		return id_transacao;
	}
	public void setId_transacao(Long id_transacao) {
		this.id_transacao = id_transacao;
	}
	public Double getValor() {
		return valor;
	}
	public void setValor(Double valor) {
		this.valor = valor;
	}
	public Integer getStatus_pagamento() {
		return status_pagamento;
	}
	public void setStatus_pagamento(Integer status_pagamento) {
		this.status_pagamento = status_pagamento;
	}
	public Long getCod_moip() {
		return cod_moip;
	}
	public void setCod_moip(Long cod_moip) {
		this.cod_moip = cod_moip;
	}
	public Integer getForma_pagamento() {
		return forma_pagamento;
	}
	public void setForma_pagamento(Integer forma_pagamento) {
		this.forma_pagamento = forma_pagamento;
	}
	public String getTipo_pagamento() {
		return tipo_pagamento;
	}
	public void setTipo_pagamento(String tipo_pagamento) {
		this.tipo_pagamento = tipo_pagamento;
	}
	public String getEmail_consumidor() {
		return email_consumidor;
	}
	public void setEmail_consumidor(String email_consumidor) {
		this.email_consumidor = email_consumidor;
	}
	
	
}
