package br.com.moip.marvel.domain;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

@Entity
public class Product {

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private Long id;
	
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	@Column(nullable = false)
	private String name;
	
	@Column(nullable = false)
	private String nameHtml;

	@Column(nullable = false)
	private String title;
	
	@Column(nullable = false)
	private String description;
	
	@Column(nullable = false)
	private Double price;
	
	@Column(nullable = false)
	private Double de;
	
	@Column(nullable = false)
	private String url;
	
	@Column(nullable = false)
	private String imagePeq;
	
	@Column(nullable = false)
	private String imageGrd;
	
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getTitle() {
		return title;
	}
	public void setTitle(String title) {
		this.title = title;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	public Double getPrice() {
		return price;
	}
	public void setPrice(Double price) {
		this.price = price;
	}
	public String getImagePeq() {
		return "/"+imagePeq;
	}
	public void setImagePeq(String imagePeq) {
		this.imagePeq = imagePeq;
	}
	public String getImageGrd() {
		return "/"+imageGrd;
	}
	public void setImageGrd(String imageGrd) {
		this.imageGrd = imageGrd;
	}
	public Double getDe() {
		return de;
	}
	public void setDe(Double de) {
		this.de = de;
	}
	public String getUrl() {
		return "/personagem/"+ url + "/" + id;
	}
	public void setUrl(String url) {
		this.url = url;
	}
	public String getNameHtml() {
		return nameHtml;
	}
	public void setNameHtml(String nameHtml) {
		this.nameHtml = nameHtml;
	}
	
	
}
