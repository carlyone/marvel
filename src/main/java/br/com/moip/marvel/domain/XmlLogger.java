package br.com.moip.marvel.domain;

import java.io.StringWriter;
import java.io.UnsupportedEncodingException;

import javax.xml.transform.OutputKeys;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.TransformerFactoryConfigurationError;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;

import org.w3c.dom.Document;

public class XmlLogger {

	public static String content(final Document doc) throws TransformerFactoryConfigurationError {
		try {
			final StringWriter writer = new StringWriter();
			final Transformer newTransformer = TransformerFactory.newInstance().newTransformer();
			newTransformer.setOutputProperty(OutputKeys.INDENT, "yes");
			newTransformer.transform(new DOMSource(doc), new StreamResult(writer));
			return new String(writer.toString().getBytes(), "UTF-8");
		} catch (final TransformerException e) {
			throw new RuntimeException("Erro ao criar o transformer", e);
		} catch (UnsupportedEncodingException e) {
			throw new RuntimeException("Erro ao criar o transformer", e);
		}
	}
	
}