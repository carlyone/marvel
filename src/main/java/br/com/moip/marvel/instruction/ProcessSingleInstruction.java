package br.com.moip.marvel.instruction;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.InputStream;
import java.io.StringReader;
import java.util.Base64;

import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.transform.Result;
import javax.xml.transform.Source;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;

import org.apache.commons.httpclient.HttpClient;
import org.apache.commons.httpclient.methods.InputStreamRequestEntity;
import org.apache.commons.httpclient.methods.PostMethod;
import org.apache.commons.io.FileUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.util.FileCopyUtils;
import org.w3c.dom.Document;
import org.xml.sax.InputSource;

import br.com.moip.marvel.xml.CreateXml;

@Component
public class ProcessSingleInstruction {

	private static final String URL = "https://desenvolvedor.moip.com.br/sandbox/ws/alpha/EnviarInstrucao/Unica";	
	
	@Autowired
	private CreateXml createXml;
	private final Logger logger = Logger.getLogger(ProcessSingleInstruction.class);

	public Document process(SingleInstruction instruction) {
		HttpClient client = new HttpClient();
		PostMethod post = new PostMethod(URL);
		String authentication = "Basic " + getAuthentication();
		
        try {
			Document doc = createXml.create(instruction);
        	byte[] copyToByteArray = FileCopyUtils.copyToByteArray(createByte(doc));
        	File fi = new File("instrucaoUnica.xml");
        	FileUtils.writeByteArrayToFile(fi, copyToByteArray);
			post.setRequestEntity(new InputStreamRequestEntity(
			        new FileInputStream(fi), fi.length()));
		} catch (Exception e1) {
			logger.error("Erro ao criar arquivo xml [Instrucao Unica].");
			return null;
		}
			
        post.setRequestHeader("Authorization", authentication);
        post.setRequestHeader("Content-type", "text/xml; charset=ISO-8859-1");
        Document document = null;
		try {
			client.executeMethod(post);			
			document = DocumentBuilderFactory.newInstance().newDocumentBuilder()
			        .parse(new InputSource(new StringReader(post.getResponseBodyAsString())));
		} catch (Exception e) {
			logger.error("Erro ao processar requisicao Instrucao Unica.");
			return null;
		}
		
		return document;
	}

	private InputStream createByte(Document doc) {
		try{
			ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
			Source xmlSource = new DOMSource(doc);
			Result outputTarget = new StreamResult(outputStream);
			TransformerFactory.newInstance().newTransformer().transform(xmlSource, outputTarget);
			InputStream is = new ByteArrayInputStream(outputStream.toByteArray());
			return is;
		} catch(Exception e){
			return null;
		}
			
	}

	private static String getAuthentication() {
		String token ="01010101010101010101010101010101";
		String key = "ABABABABABABABABABABABABABABABABABABABAB";
		
		byte[] encodedBytes = Base64.getEncoder().encode((token + ":" + key).getBytes());
		
		return new String(encodedBytes);
	}
}
