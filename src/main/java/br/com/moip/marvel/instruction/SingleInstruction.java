package br.com.moip.marvel.instruction;

import br.com.moip.marvel.domain.Payment;

public class SingleInstruction {
	
	private String razao;
	private Double valor;
	private Payment payment;

	public SingleInstruction(String razao, Double valor, Payment payment) {
		this.razao = razao;
		this.valor = valor;
		this.payment = payment;
	}
	
	public String getRazao() {
		return razao;
	}
	public void setRazao(String razao) {
		this.razao = razao;
	}
	public Double getValor() {
		return valor;
	}
	public void setValor(Double valor) {
		this.valor = valor;
	}
	public Payment getPayment() {
		return payment;
	}
	public void setPayment(Payment payment) {
		this.payment = payment;
	}
	
	
}
