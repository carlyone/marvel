package br.com.moip.marvel.services;

import java.io.File;
import java.io.FileReader;
import java.io.IOException;

import org.springframework.core.io.ClassPathResource;
import org.springframework.core.io.Resource;

import com.google.gson.JsonObject;
import com.google.gson.JsonParser;

public class ProductFile {

	private static JsonObject jsonProduct;
	
	public static JsonObject getJsonProduct() {
		if(jsonProduct == null) {
			jsonProduct = getJson();
		}
		return jsonProduct;
	}

	private static JsonObject getJson() {
		Resource resource = new ClassPathResource("products.json");
		JsonObject json = null;
	    try {
			File file = resource.getFile();
			JsonParser parser = new JsonParser();
			json = (JsonObject) parser.parse(new FileReader(file));
			
		} catch (IOException e) {
			return json;
		}
		return json;
	}
}
