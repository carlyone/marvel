package br.com.moip.marvel.services;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import br.com.moip.marvel.domain.Product;
import br.com.moip.marvel.domain.ProductRepository;

@Repository
public class ProductService {

	@Autowired
	private ProductRepository productRepository;
	
	public List<Product> findAll(){
		return productRepository.findAll();
	}
	
	public Product findOne(long id){
		return productRepository.findOne(id);
	}

}
