package br.com.moip.marvel.shopping;

import java.util.ArrayList;
import java.util.List;

import org.springframework.context.annotation.Scope;
import org.springframework.context.annotation.ScopedProxyMode;
import org.springframework.stereotype.Component;
import org.springframework.web.context.WebApplicationContext;

import br.com.moip.marvel.domain.Product;

@Component
@Scope(value=WebApplicationContext.SCOPE_SESSION, proxyMode=ScopedProxyMode.TARGET_CLASS)
public class ShoppingCart {

	private List<Product> products;

	public List<Product> getProducts() {
		if (products == null) {
			products = new ArrayList<>();
		}
		return products;
	}

	public void setProducts(List<Product> products) {
		this.products = products;
	}
	
	public Double getTotalDiscount(boolean isDiscount) {
		Double total = getTotal();
		
		if(isDiscount) {
			total -= total * 0.05;
		}
		
		return total;
	}
	
	public Double getTotal() {
		Double total = 0.0;
		
		for(Product p: products) {
			total += p.getPrice();
		}
		
		return total;
	}
}
