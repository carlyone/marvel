package br.com.moip.marvel.web;

import static org.springframework.util.StringUtils.isEmpty;
import static org.springframework.web.bind.annotation.RequestMethod.POST;

import javax.servlet.http.HttpSession;
import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.RequestMapping;

import br.com.moip.marvel.domain.Payment;
import br.com.moip.marvel.instruction.SingleInstruction;
import br.com.moip.marvel.shopping.ShoppingCart;
import br.com.moip.marvel.xml.RedirectTokenXml;

@Controller
public class PaymentController {
	
	@Autowired
	RedirectTokenXml redirectToken;
	
	private static final String RAZAO = "E-commerce Carlyone";
	
	@RequestMapping(value="/pagamento", method=POST)
	public String payment(@Valid Payment payment, BindingResult bindingResult, HttpSession session){

		if(bindingResult.hasErrors()){
			return "payment";
		}
		
		ShoppingCart shoppingCart = (ShoppingCart) session.getAttribute("shoppingCart");
		Double total = shoppingCart.getTotalDiscount(!isEmpty(payment.getCuponCode()));
		
		SingleInstruction instruction = new SingleInstruction(RAZAO, total, payment);
		String redirectUrl = redirectToken.redirect(instruction);
		
		session.removeAttribute("shoppingCart");
		return "redirect:" + redirectUrl;
	}
	
	@RequestMapping("/pagamento-finalizado")
	public String paymentCompleted() {
		return "paymentCompleted";
	}
}
