package br.com.moip.marvel.web;

import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import br.com.moip.marvel.domain.Product;
import br.com.moip.marvel.services.ProductService;

@Controller
public class PersonageController {

	@Autowired
	private ProductService productService;
	
	@RequestMapping("/")
	public ModelAndView index(HttpSession session) {        
		ModelAndView mv = new ModelAndView("index");
	    
        mv.addObject("products", productService.findAll());
		mv.addObject("kitCompleto", productService.findOne(5));

		return mv;
	}
	
	@RequestMapping("/personagem/{personage}/{id}")
	public ModelAndView personage(HttpSession session ,@PathVariable("id") String id) {
		ModelAndView mv = new ModelAndView("personage");
		Product product = productService.findOne(new Long(id));

		mv.addObject("product", product);
		
		return mv;
	}
}
