package br.com.moip.marvel.web;

import java.util.List;

import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.context.WebApplicationContext;
import org.springframework.web.servlet.ModelAndView;

import br.com.moip.marvel.domain.Payment;
import br.com.moip.marvel.domain.Product;
import br.com.moip.marvel.services.ProductService;
import br.com.moip.marvel.shopping.ShoppingCart;

@Controller
@Scope(value=WebApplicationContext.SCOPE_REQUEST)
public class ShoppingCartController {

	private static final String SHOPPINGCART = "shoppingCart";
	private static final String VIEW = "carrinho";
	
	@Autowired
	private ProductService productService;
	
	@RequestMapping(value="/addProduto", method=RequestMethod.POST)
	public String add(HttpSession session, String id){
		ShoppingCart cart  = (ShoppingCart) session.getAttribute(SHOPPINGCART);
		
		if(cart == null) {
			cart = new ShoppingCart();
		}
		
		List<Product> products = cart.getProducts();
		products.add(productService.findOne(new Long(id)));
		
		session.setAttribute(SHOPPINGCART, cart);
		
		return "redirect:/meu-carrinho";
	}
	
	@RequestMapping("/finalizar-compra")
	public String finalizarCompra(Model model){
		model.addAttribute("payment", new Payment());
		return "payment";
	}
	
	@RequestMapping("/meu-carrinho")
	public ModelAndView carrinho() {
		ModelAndView mv = new ModelAndView(VIEW);
		mv.addObject("meuCarrinho", true);
		return mv;
	}
	
	@RequestMapping("/remove")
	public String remove(HttpSession session){
		session.removeAttribute(SHOPPINGCART);
		
		return "redirect:/meu-carrinho";
	}

}
