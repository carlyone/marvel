package br.com.moip.marvel.web;

import static org.springframework.web.bind.annotation.RequestMethod.POST;

import org.apache.log4j.Logger;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;

import br.com.moip.marvel.domain.PaymentResponse;

@Controller
public class UpdatePaymentController {

	private final Logger logger = Logger.getLogger(UpdatePaymentController.class);
	
	@RequestMapping(value="/update-payment", method = POST)
	public void update(@RequestBody PaymentResponse paymentResponse) {
		logger.info("Atualizando informacoes de pagamento");
	}
}
