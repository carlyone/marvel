package br.com.moip.marvel.xml;

import org.springframework.stereotype.Component;
import org.w3c.dom.Document;
import org.w3c.dom.NodeList;

@Component
public class BuildXmlToken {
	
	public String getToken(Document doc) {
		NodeList nodeList = doc.getElementsByTagName("Token");
		
		if(nodeList.getLength() > 0) {
			return nodeList.item(0).getTextContent();
		}
		
		return "";
	}
	
}
