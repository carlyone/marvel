package br.com.moip.marvel.xml;

import static br.com.moip.marvel.xml.SingleInstructionXml.BOLETO;
import static br.com.moip.marvel.xml.SingleInstructionXml.DIASEXPIRACAO;
import static br.com.moip.marvel.xml.SingleInstructionXml.ENVIARINSTRUCAO;
import static br.com.moip.marvel.xml.SingleInstructionXml.FORMAPAGAMENTO;
import static br.com.moip.marvel.xml.SingleInstructionXml.FORMASPAGAMENTOS;
import static br.com.moip.marvel.xml.SingleInstructionXml.IDPROPRIO;
import static br.com.moip.marvel.xml.SingleInstructionXml.INSTRUCAOUNICA;
import static br.com.moip.marvel.xml.SingleInstructionXml.JUROS;
import static br.com.moip.marvel.xml.SingleInstructionXml.MAXIMOPARCELAS;
import static br.com.moip.marvel.xml.SingleInstructionXml.MINIMOPARCELAS;
import static br.com.moip.marvel.xml.SingleInstructionXml.MOEDA;
import static br.com.moip.marvel.xml.SingleInstructionXml.PARCELAMENTO;
import static br.com.moip.marvel.xml.SingleInstructionXml.PARCELAMENTOS;
import static br.com.moip.marvel.xml.SingleInstructionXml.QTDDIASBOLETO;
import static br.com.moip.marvel.xml.SingleInstructionXml.RAZAO;
import static br.com.moip.marvel.xml.SingleInstructionXml.RECEBIMENTO;
import static br.com.moip.marvel.xml.SingleInstructionXml.TIPO;
import static br.com.moip.marvel.xml.SingleInstructionXml.TIPOBOLETO;
import static br.com.moip.marvel.xml.SingleInstructionXml.TIPOMOEDA;
import static br.com.moip.marvel.xml.SingleInstructionXml.URLRETORNO;
import static br.com.moip.marvel.xml.SingleInstructionXml.VALOR;
import static br.com.moip.marvel.xml.SingleInstructionXml.VALORES;
import static java.lang.String.valueOf;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;

import org.apache.log4j.Logger;
import org.springframework.stereotype.Component;
import org.w3c.dom.Attr;
import org.w3c.dom.Document;
import org.w3c.dom.Element;

import br.com.moip.marvel.instruction.SingleInstruction;

@Component
public class CreateXml {

	private final Logger logger = Logger.getLogger(CreateXml.class);

	public Document create(SingleInstruction instruction) {
		try {
			DocumentBuilderFactory docFactory = DocumentBuilderFactory.newInstance();
			DocumentBuilder docBuilder = docFactory.newDocumentBuilder();
	
			Document doc = docBuilder.newDocument();
			Element enviarInstruca = doc.createElement(ENVIARINSTRUCAO);
			doc.appendChild(enviarInstruca);
			
			Element instrucaoUnica = doc.createElement(INSTRUCAOUNICA);
			enviarInstruca.appendChild(instrucaoUnica);
	
			Element razao = doc.createElement(RAZAO);
			instrucaoUnica.appendChild(razao);
			razao.appendChild(doc.createTextNode(instruction.getRazao()));
			
			Element idProprio = doc.createElement(IDPROPRIO);
			instrucaoUnica.appendChild(idProprio);
			idProprio.appendChild(doc.createTextNode(generateId()));
			
			Element formasPagamentos = doc.createElement(FORMASPAGAMENTOS);
			instrucaoUnica.appendChild(formasPagamentos);

			createFormaPagamento(formasPagamentos, doc, "BoletoBancario", FORMAPAGAMENTO);
			createFormaPagamento(formasPagamentos, doc, "CarteiraMoIP", FORMAPAGAMENTO);
			createFormaPagamento(formasPagamentos, doc, "CartaoCredito", FORMAPAGAMENTO);
			createFormaPagamento(formasPagamentos, doc, "DebitoBancario", FORMAPAGAMENTO);

			Element boleto = doc.createElement(BOLETO);
			instrucaoUnica.appendChild(boleto);

			Element diasExpiracao = doc.createElement(DIASEXPIRACAO);
			boleto.appendChild(diasExpiracao);

			Attr attrBoleto = doc.createAttribute(TIPO);
			attrBoleto.setValue(TIPOBOLETO);
			diasExpiracao.setAttributeNode(attrBoleto);
			diasExpiracao.appendChild(doc.createTextNode(QTDDIASBOLETO));
			
			Element parcelamentos = doc.createElement(PARCELAMENTOS);
			instrucaoUnica.appendChild(parcelamentos);

			Element parcelamento = doc.createElement(PARCELAMENTO);
			parcelamentos.appendChild(parcelamento);

			createFormaPagamento(parcelamento, doc, "2", MINIMOPARCELAS);
			createFormaPagamento(parcelamento, doc, "5", MAXIMOPARCELAS);
			createFormaPagamento(parcelamento, doc, "Parcelado", RECEBIMENTO);
			createFormaPagamento(parcelamento, doc, "2.5", JUROS);

			Element valores = doc.createElement(VALORES);
			instrucaoUnica.appendChild(valores);
			
			Element valor = doc.createElement(VALOR);
			valores.appendChild(valor);
			Attr attr = doc.createAttribute(MOEDA);
			attr.setValue(TIPOMOEDA);
			valor.setAttributeNode(attr);
			valor.appendChild(doc.createTextNode(valueOf(instruction.getValor())));
			
			Element urlRetorno = doc.createElement(URLRETORNO);
			instrucaoUnica.appendChild(urlRetorno);
			urlRetorno.appendChild(doc.createTextNode("http://52.67.250.232/pagamento-finalizado"));
			
			return doc;
		}catch(Exception e){
			logger.error("Erro ao criar XML: " + e.getMessage());
			return null;
		}
	}
	
	private void createFormaPagamento(Element root, Document doc, String value, String textElement) {
		Element formaPagamento = doc.createElement(textElement);
		root.appendChild(formaPagamento);
		formaPagamento.appendChild(doc.createTextNode(value));
	}
	
	private String generateId() {
		return "shoppingCarlyone" + System.nanoTime() ;
	}
	
}
