package br.com.moip.marvel.xml;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.w3c.dom.Document;

import br.com.moip.marvel.instruction.ProcessSingleInstruction;
import br.com.moip.marvel.instruction.SingleInstruction;

@Component
public class RedirectTokenXml {

	private static final String URL_REDIRECT = "https://desenvolvedor.moip.com.br/sandbox/Instrucao.do?token=##";
	
	@Autowired
	private ProcessSingleInstruction process;
	
	@Autowired
	private BuildXmlToken buildXmlToken;
	
	public String redirect(SingleInstruction instruction) {
		Document doc = process.process(instruction);
		String token = buildXmlToken.getToken(doc);
		
		return URL_REDIRECT.replace("##", token);
	}
	
}
