package br.com.moip.marvel.xml;

public class SingleInstructionXml {

	public static final String ENVIARINSTRUCAO = "EnviarInstrucao";
	public static final String INSTRUCAOUNICA = "InstrucaoUnica";
	public static final String RAZAO = "Razao";
	public static final String IDPROPRIO = "IdProprio";
	public static final String VALORES = "Valores";
	public static final String VALOR = "Valor";
	public static final String MOEDA = "Moeda";
	public static final String TIPOMOEDA = "BRL";
	public static final String TIPOBOLETO = "Corridos";
	public static final String TIPO = "Tipo";
	public static final String FORMASPAGAMENTOS = "FormasPagamentos";
	public static final String FORMAPAGAMENTO = "FormaPagamento";
	public static final String BOLETO = "Boleto";
	public static final String DIASEXPIRACAO = "DiasExpiracao";
	public static final String QTDDIASBOLETO = "5";
	public static final String PARCELAMENTOS = "Parcelamentos";
	public static final String PARCELAMENTO = "Parcelamento";
	public static final String MINIMOPARCELAS = "MinimoParcelas";
	public static final String MAXIMOPARCELAS = "MaximoParcelas";
	public static final String RECEBIMENTO = "Recebimento";
	public static final String JUROS = "Juros";
	public static final String URLRETORNO = "URLRetorno";
}
