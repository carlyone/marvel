package br.com.moip.marvel.xml;

import java.io.File;

import javax.xml.parsers.DocumentBuilderFactory;

import org.springframework.core.io.ClassPathResource;
import org.springframework.core.io.Resource;
import org.w3c.dom.Document;

public class XmlResource {
	
	public static Document getXml(Class<?> classe, String fileName){
		try {
			Resource resource = new ClassPathResource(fileName);
			File file = null;
			file = resource.getFile();
			return DocumentBuilderFactory.newInstance().newDocumentBuilder().parse(file);
		} catch (Exception e) {
			return null;
		}
	}
}
