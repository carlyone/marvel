package br.com.moip.marvel.instruction;

import static br.com.moip.marvel.xml.SingleInstructionXml.DIASEXPIRACAO;
import static br.com.moip.marvel.xml.SingleInstructionXml.ENVIARINSTRUCAO;
import static br.com.moip.marvel.xml.SingleInstructionXml.FORMAPAGAMENTO;
import static br.com.moip.marvel.xml.SingleInstructionXml.FORMASPAGAMENTOS;
import static br.com.moip.marvel.xml.SingleInstructionXml.IDPROPRIO;
import static br.com.moip.marvel.xml.SingleInstructionXml.INSTRUCAOUNICA;
import static br.com.moip.marvel.xml.SingleInstructionXml.JUROS;
import static br.com.moip.marvel.xml.SingleInstructionXml.MAXIMOPARCELAS;
import static br.com.moip.marvel.xml.SingleInstructionXml.MINIMOPARCELAS;
import static br.com.moip.marvel.xml.SingleInstructionXml.PARCELAMENTOS;
import static br.com.moip.marvel.xml.SingleInstructionXml.RAZAO;
import static br.com.moip.marvel.xml.SingleInstructionXml.RECEBIMENTO;
import static br.com.moip.marvel.xml.SingleInstructionXml.URLRETORNO;
import static br.com.moip.marvel.xml.SingleInstructionXml.VALOR;
import static br.com.moip.marvel.xml.SingleInstructionXml.VALORES;
import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.when;
import static org.mockito.MockitoAnnotations.initMocks;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;
import org.w3c.dom.Document;
import org.w3c.dom.Node;

import br.com.moip.marvel.ConfigurationSpringBoot;
import br.com.moip.marvel.xml.CreateXml;

@RunWith(SpringRunner.class)
@SpringBootTest(classes = ConfigurationSpringBoot.class)
public class CreateXMLTest {

	@Autowired
	private CreateXml createXml;
	
	@Mock
	private SingleInstruction instruction;
	
	@Before
	public void setup() {
		initMocks(this);
		when(instruction.getRazao()).thenReturn("E-Commerce Carlyone");
		when(instruction.getValor()).thenReturn(1000.0);
	}

	@Test
	public void validElementsRequerid() {
		Document create = createXml.create(instruction);
		assertEquals(1, create.getElementsByTagName(ENVIARINSTRUCAO).getLength());
		assertEquals(1, create.getElementsByTagName(INSTRUCAOUNICA).getLength());
		assertEquals(1, create.getElementsByTagName(RAZAO).getLength());
		assertEquals(1, create.getElementsByTagName(IDPROPRIO).getLength());
		assertEquals(1, create.getElementsByTagName(VALORES).getLength());
		assertEquals(1, create.getElementsByTagName(FORMASPAGAMENTOS).getLength());
		assertEquals(4, create.getElementsByTagName(FORMAPAGAMENTO).getLength());
		assertEquals(1, create.getElementsByTagName(DIASEXPIRACAO).getLength());
		assertEquals(1, create.getElementsByTagName(PARCELAMENTOS).getLength());
		assertEquals(1, create.getElementsByTagName(PARCELAMENTOS).getLength());
		assertEquals(1, create.getElementsByTagName(MINIMOPARCELAS).getLength());
		assertEquals(1, create.getElementsByTagName(MAXIMOPARCELAS).getLength());
		assertEquals(1, create.getElementsByTagName(RECEBIMENTO).getLength());
		assertEquals(1, create.getElementsByTagName(JUROS).getLength());
		assertEquals(1, create.getElementsByTagName(URLRETORNO).getLength());
	}
	
	@Test
	public void valueXmlEquals1000() {
		Document create = createXml.create(instruction);
		Node item = create.getElementsByTagName(VALOR).item(0);
		assertEquals("1000.0", item.getFirstChild().getNodeValue());
	}

}
