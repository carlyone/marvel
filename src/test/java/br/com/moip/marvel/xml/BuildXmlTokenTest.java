package br.com.moip.marvel.xml;

import static org.junit.Assert.assertEquals;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;
import org.w3c.dom.Document;

import br.com.moip.marvel.ConfigurationSpringBoot;

@RunWith(SpringRunner.class)
@SpringBootTest(classes = ConfigurationSpringBoot.class)
public class BuildXmlTokenTest {
	
	@Autowired
	private BuildXmlToken buildXmlToken;	

	private Document doc;
	
	@Before
	public void setup() {
		doc = XmlResource.getXml(BuildXmlTokenTest.class, "responseInstrucaoUnicaTeste.xml");
	}
	
	@Test
	public void getToken() {
		assertEquals("T2N0L0X8E0S71217U2H3W1T4F4S4G4K731D010V0S0V0S080M010E0Q082X2", buildXmlToken.getToken(doc));
	}

}
